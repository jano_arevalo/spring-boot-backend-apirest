package cl.singularyti.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.singularyti.models.entity.Region;

public interface IRegionDao extends JpaRepository<Region, Long> {}
