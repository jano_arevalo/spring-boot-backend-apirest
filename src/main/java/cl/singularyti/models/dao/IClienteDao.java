package cl.singularyti.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.singularyti.models.entity.Cliente;

public interface IClienteDao extends JpaRepository<Cliente, Long>{}
