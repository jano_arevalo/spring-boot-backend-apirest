package cl.singularyti.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.singularyti.models.dao.IClienteDao;
import cl.singularyti.models.dao.IRegionDao;
import cl.singularyti.models.entity.Cliente;
import cl.singularyti.models.entity.Region;

@Service
public class ClienteServiceImpl implements IClienteService {

  @Autowired private IClienteDao clienteDao;

  @Autowired private IRegionDao regionDao;

  @Override
  @Transactional(readOnly = true)
  public List<Cliente> findAll() {
    return clienteDao.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Cliente> findAll(Pageable pageable) {
    return clienteDao.findAll(pageable);
  }

  @Override
  @Transactional
  public void save(Cliente cliente) {
    clienteDao.save(cliente);
  }

  @Override
  @Transactional(readOnly = true)
  public Cliente findById(Long id) {
    return clienteDao.findById(id).orElse(null);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    clienteDao.deleteById(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Region> findAllRegiones() {
    return regionDao.findAll();
  }
}
