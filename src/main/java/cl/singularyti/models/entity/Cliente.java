package cl.singularyti.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(
    name = "clientes",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "email"})})
public class Cliente implements Serializable {

  /** */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @NotEmpty(message = "No puede estar vacío")
  @Size(min = 4, max = 12, message = "El tamaño debe estar entre 4 y 12")
  private String nombre;

  @NotNull
  @NotEmpty(message = "No puede estar vacío")
  private String apellido;

  @Email(message = "No es una dirección de correo bien formada")
  @NotEmpty(message = "No puede estar vacío")
  @NotNull
  private String email;

  @NotNull
  @NotEmpty(message = "No puede estar vacío")
  @Temporal(TemporalType.DATE)
  @Column(name = "created_at")
  private Date createdAt;

  private String foto;

  @NotNull(message = "La región no puede estar vacía")
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonBackReference
  private Region region;

  public Cliente() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getFoto() {
    return foto;
  }

  public void setFoto(String foto) {
    this.foto = foto;
  }

  public Region getRegion() {
    return region;
  }

  public void setRegion(Region region) {
    this.region = region;
  }
}
