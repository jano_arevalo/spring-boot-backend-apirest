package cl.singularyti.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.singularyti.models.entity.Cliente;
import cl.singularyti.models.service.IClienteService;

@RestController
@RequestMapping("/api/clientes")
public class ClienteRestController {

  @Autowired private IClienteService clienteService;

  @GetMapping({"", "/"})
  public List<Cliente> index() {
    return clienteService.findAll();
  }
}
