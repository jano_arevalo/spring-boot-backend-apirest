package cl.singularyti.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.singularyti.models.dao.IRegionDao;
import cl.singularyti.models.entity.Region;

@RestController
@RequestMapping("/api/regiones")
public class RegionRestController {

  @Autowired private IRegionDao regionDao;

  @GetMapping({"", "/"})
  public List<Region> index() {
    return regionDao.findAll();
  }
}
